#!/usr/bin/zsh

# GIVEN list of file names
# WHEN lua file exists in uncompressed directory
# THEN compression of lua file made in compressed directory

oeo=("we_start" "ww_decompressing" "ww_parser" "ww_parsing" "we_parsing" "we_decompressing" "uw_outputting" "strung" "jw_configuring" "xwx_converting")
for owo ($oeo); do
    echo $owo
    luasrcdiet ./iei/$owo.lua -o ./iqi/$owo.lua
done

# AND Application launched

echo "iei"
ls -la iei/

echo "iqi"
ls -la iqi/
fe=we_start.lua
cd iqi
time luajit $fe | head -n 1 | jq '. | .[1]'
cd ..
